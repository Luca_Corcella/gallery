import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {GalleryModel, GalleryService} from "./gallery.service";
import {TestBed} from "@angular/core/testing";
import {apiMokResponse, responseMock} from "./mock";


describe('GalleryService', () => {

    let httpClient: HttpClient;
    let httpTestingController: HttpTestingController;
    let galleryService: GalleryService;

  beforeEach(() => {
    //Configures testing app module
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        GalleryService
      ]
    });
    //Instantaites HttpClient, HttpTestingController and GalleryService
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    galleryService = TestBed.inject(GalleryService);
  });


  afterEach(() => {
    httpTestingController.verify(); //Verifies that no requests are outstanding.
  });

  describe('#fetchGalleryPics', () => {
    let expectedGalleryImg;
    beforeEach(() => {
      //Dummy data to be returned by request.
       expectedGalleryImg = responseMock as GalleryModel[];
    });
  });

  //Test case 1
  it('should return expected images by calling once', () => {
    let expectedGalleryImg = responseMock as GalleryModel[];
    galleryService.fetchGalleryPics({}, 'pics').subscribe(img => {
        expect(img).toEqual(expectedGalleryImg, 'should return expected pics')
      },
      fail
    );

    const req = httpTestingController.expectOne('https://www.reddit.com/r/pics/top.json?limit=10');
    expect(req.request.method).toEqual('GET');

    req.flush(apiMokResponse); //Return IMG from api
  });

  //Test case 2
  it('should be OK returning no pics', () => {

    galleryService.fetchGalleryPics({}, 'null').subscribe(
      (img:any) => expect(img.data?.children.length).toEqual( 0 || undefined, 'should have empty  array'),
      fail
    );

    const req = httpTestingController.expectOne('https://www.reddit.com/r/null/top.json?limit=10');
    req.flush([]); //Return empty data
  });

})
