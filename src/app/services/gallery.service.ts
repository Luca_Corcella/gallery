import {Injectable} from "@angular/core";
import {HttpClient} from '@angular/common/http';
import {catchError, map, take} from "rxjs/operators";
import {BehaviorSubject, Observable, of} from "rxjs";
import {Child, RedditApiResponse} from "./model";

export interface GalleryModel {
  id: string,
  imgSrc: string,
  title: string,
  link: string
}

@Injectable({
  providedIn: 'root'
})
export class GalleryService {
  URL: string = 'https://www.reddit.com';

  private dataRout$: BehaviorSubject<GalleryModel> = new BehaviorSubject<GalleryModel>({} as GalleryModel);

  constructor(private http: HttpClient) {
  }

  /**
   * fatch api from reddit
   * @param params Obj
   * @param search Boolean
   */
  fetchGalleryPics(params: any, keyWord: string = 'pics'): Observable<GalleryModel[]> {
    return this.http.get<RedditApiResponse>(`${this.URL}/r/${keyWord}/top.json?limit=10`, {params: params}).pipe(
      map((res: RedditApiResponse) => res.data.children.filter((img: Child) => !!this.isImg(img.data.url) || (img.data.preview && !!this.isImg(img.data?.preview?.images[0].source.url.replace('amp;s', 's')))).map((img: Child) => ({
        imgSrc: img.data.url,
        title: img.data.title,
        link: `${this.URL}${img.data.permalink}`,
        id: img.data.id,
      }))),
      catchError(e => of(e))
    )
  }

  /**
   * sava data rout in localstorage
   * @param data
   */
  setDataRout(data: GalleryModel): void {
    localStorage.setItem('data-rout', JSON.stringify(data))
    this.dataRout$.next(data)
  }

  /**
   * return data from rout when user clicked on img
   */
  getDataRout() {
    return this.dataRout$.pipe(
      map(value => {
        if (localStorage.getItem('data-rout') !== undefined) {
          const data = localStorage.getItem('data-rout');
          return JSON.parse(<string>data)
        } else {
          return value
        }
      }),
      take(1)
    )
  }

  /**
   * check if url is an img
   * @param item
   */
  isImg(item: string): boolean {
    if (item.includes('.png') || item.includes('.jpg') || item.includes('.jpeg') || item.includes('.gif')) {
      return true;
    }
    return false;
  }
}
