import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./features/gallery/gallery.module').then(module => module.GalleryModule)
  },
  {
    path: 'details',
    data: {},
    loadChildren: () => import('./features/details-img/details-img.module').then(module => module.DetailsImgModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{scrollPositionRestoration: 'top'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
