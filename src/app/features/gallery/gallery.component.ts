import {Component, HostListener, OnDestroy, OnInit} from '@angular/core';
import {GalleryModel, GalleryService} from "../../services/gallery.service";
import {Subscription} from "rxjs";
import {FormControl} from "@angular/forms";
import {debounceTime, distinctUntilChanged, startWith} from "rxjs/operators";
import {Router} from "@angular/router";
import {animate, query, stagger, style, transition, trigger} from "@angular/animations";


export const fadeAnimation = trigger('listAnimation', [
  transition('* <=> *', [
    query(':enter',
      [style({ opacity: 0 }), stagger('800ms', animate('900ms ease-out', style({ opacity: 1 })))],
      { optional: true }
    ),
    query(':leave',
      animate('800ms', style({ opacity: 0 })),
      { optional: true }
    )
  ])
]);

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [fadeAnimation],
})
export class GalleryComponent implements OnInit, OnDestroy {

  galleryImg: GalleryModel[]  = [];
  gallerySubscriber$: Subscription | undefined;
  lastId: string | null = null;

  searchInput = new FormControl(['pics']);
  field: string | null = null;
  noSearchResult: boolean = false;

  /**
   * handler end of scroll page
   */
  @HostListener('window:scroll', ['$event'])
  onWindowScroll() {
    if (window.innerHeight + window.scrollY === document.body.scrollHeight) {
      if(!!this.lastId) {
        this.field = this.field ?? 'pics';
        this.loadImg({after: `t3_${this.lastId}`}, this.field);
      }
    }
  }

  constructor(private srv: GalleryService, private router: Router) {}

  ngOnInit(): void {
    /**
     * handler when search input change status and user insert values
     */
    this.searchInput.valueChanges.pipe(
      startWith(''),
      debounceTime(800),
      distinctUntilChanged(),
    ).subscribe((text) => {
      this.galleryImg = [];
      this.field = text !== '' ? text : null;
      if(text === '') {
        this.loadImg({after: ``}); // load default pics
      } else {
        this.loadImg({after: ``}, text);
      }
    })
  }

  /**
   * load images from ApI
   * @param params
   */
  loadImg(params: any, text?: string): void {
    this.gallerySubscriber$ = this.srv.fetchGalleryPics(params, text).subscribe(img => {
      if(img && img.length > 0) {
        this.noSearchResult = false;
        this.galleryImg = [...this.galleryImg ,...img];
        this.lastId = img[img.length -1]?.id ?? null
      } else {
        this.noSearchResult = true;
      }
    });
  }

  /**
   * show details and full-screen Image
   * @param data
   */
  selectImg(data: GalleryModel): void {
    this.srv.setDataRout(data)
    this.router.navigate(['details'])
  }

  ngOnDestroy(): void {
    if(this.gallerySubscriber$) {
      this.gallerySubscriber$.unsubscribe()
    }
  }

}
