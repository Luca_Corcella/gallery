import {ComponentFixture, TestBed} from '@angular/core/testing';
import {GalleryComponent} from './gallery.component';
import {GalleryModel, GalleryService} from "../../services/gallery.service";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../shared/shared.module";
import {SearchBoxComponent} from "./components/search-box/search-box.component";
import {CUSTOM_ELEMENTS_SCHEMA} from "@angular/core";
import {responseMock} from "../../services/mock";
import {BehaviorSubject, of} from "rxjs";
import {ImageBoxComponent} from "./components/image-box/image-box.component";
import {By} from "@angular/platform-browser";

describe('GalleryComponent', () => {

  let expectedGalleryImg = responseMock as GalleryModel[];
  let galleryImgData$: BehaviorSubject<GalleryModel[]>;

  let component: GalleryComponent;
  let fixture: ComponentFixture<GalleryComponent>;

  beforeEach(async () => {
    galleryImgData$ = new BehaviorSubject<GalleryModel[]>(expectedGalleryImg);
    await TestBed.configureTestingModule({
      declarations: [GalleryComponent, SearchBoxComponent,ImageBoxComponent],
      imports: [BrowserAnimationsModule, HttpClientModule, RouterModule, RouterTestingModule, SharedModule],
      providers: [HttpClient, GalleryService],
      schemas:[CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryComponent);
    component = fixture.componentInstance;

    component.galleryImg = expectedGalleryImg;
    fixture.detectChanges();

  });

  it('should create container', () => {
    expect(component).toBeDefined();
    expect(component).toBeTruthy();
  });


  it('should be a img-box', () => {
    const {debugElement} = fixture;
    const elImgBox = debugElement.query(By.css('.img-box'));
    // console.log(elImgBox)
    expect(elImgBox).not.toBeUndefined()
  });


  beforeEach(() => {
    let expectedGalleryImg = responseMock as GalleryModel[];
    let galleryService = fixture.debugElement.injector.get(GalleryService);
    let spy = spyOn(galleryService, 'fetchGalleryPics')
      .and.returnValue(of(expectedGalleryImg));
  });

});
