import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
  selector: 'image-box',
  template: `
    <img src="{{src}}" alt="{{title}}" loading="lazy">
  `,
  styleUrls: ['./image-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageBoxComponent {
  @Input() src: string = '';
  @Input() title: string = '';
}
