import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageBoxComponent } from './image-box.component';
import {By} from "@angular/platform-browser";

describe('ImageBoxComponent', () => {
  let component: ImageBoxComponent;
  let fixture: ComponentFixture<ImageBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageBoxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {

    let expectedImg = {
      "imgSrc": "https://i.redd.it/f0gkowkt81771.jpg",
      "title": "Taken moments after Billy Dee told Mark direct eye contact w royalty punishable by prison/beheading",
      "link": "https://www.reddit.com/r/pics/comments/o6ez2w/taken_moments_after_billy_dee_told_mark_direct/",
      "id": "o6ez2w"
    }

    component.title = expectedImg.title;
    component.src = expectedImg.imgSrc;

    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

});
