import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'search-box',
  template: `
    <mat-form-field>
      <input matInput [formControl]="inputCtrl" placeholder="Search a Image">
    </mat-form-field>
  `,
  styleUrls: ['./search-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchBoxComponent {

  @Input() inputCtrl: FormControl = new FormControl();

}
