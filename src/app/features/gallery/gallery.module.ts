import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { ImageBoxComponent } from './components/image-box/image-box.component';
import { SearchBoxComponent } from './components/search-box/search-box.component';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
  declarations: [
    GalleryComponent,
    ImageBoxComponent,
    SearchBoxComponent
  ],
  imports: [
    CommonModule,
    GalleryRoutingModule,
    SharedModule
  ]
})
export class GalleryModule { }
