import { Component } from '@angular/core';
import {Router} from "@angular/router";
import {GalleryModel, GalleryService} from "../../services/gallery.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-details-img',
  templateUrl: './details-img.component.html',
  styleUrls: ['./details-img.component.scss']
})
export class DetailsImgComponent {

  data$: Observable<GalleryModel> = this.srv.getDataRout();

  constructor(private router: Router, private srv: GalleryService,) {}

}
