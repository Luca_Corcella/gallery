import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DetailsImgComponent} from "./details-img.component";

const routes: Routes = [
  {
    path: '',
    component: DetailsImgComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsImgRoutingModule { }
