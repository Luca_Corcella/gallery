import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailsImgRoutingModule } from './details-img-routing.module';
import { DetailsImgComponent } from './details-img.component';
import {SharedModule} from "../../shared/shared.module";


@NgModule({
  declarations: [
    DetailsImgComponent
  ],
  imports: [
    CommonModule,
    DetailsImgRoutingModule,
    SharedModule
  ]
})
export class DetailsImgModule { }
