import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsImgComponent } from './details-img.component';
import {GalleryService} from "../../services/gallery.service";
import {RouterModule} from "@angular/router";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {RouterTestingModule} from "@angular/router/testing";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {SharedModule} from "../../shared/shared.module";
import {of} from "rxjs";
import {responseMock} from "../../services/mock";

describe('DetailsImgComponent', () => {
  let component: DetailsImgComponent;
  let fixture: ComponentFixture<DetailsImgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsImgComponent ],
      imports: [BrowserAnimationsModule, HttpClientModule, RouterModule, RouterTestingModule, SharedModule],
      providers: [HttpClient, GalleryService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsImgComponent);
    component = fixture.componentInstance;

    component.data$ = of({
      "imgSrc": "https://i.redd.it/f0gkowkt81771.jpg",
      "title": "Taken moments after Billy Dee told Mark direct eye contact w royalty punishable by prison/beheading",
      "link": "https://www.reddit.com/r/pics/comments/o6ez2w/taken_moments_after_billy_dee_told_mark_direct/",
      "id": "o6ez2w"
    })
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
